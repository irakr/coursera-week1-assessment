/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief Week-1 assessment
 *
 * Basic application to perform statistical analytics on a dataset including median, mean,
 * maximum and minimum
 *
 * @author Irak Rigia
 * @date 25/08/2017
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/**
 * @brief Print the statistical analytics data of an array
 *
 * This function takes an array of 8-bit unsigned integers and prints out various
 * statistical analytics data on the screen including minimum, maximum, mean, and median.
 * It does all these by calling the other functions that computes individual statitics.
 * For example: It calls the function find_median() to compute the median of the array.
 *
 * @param array An array of unsigned char values of which the statistics have to be printed
 * @param size Number of elements in the parameter array
 *
 * @return None
 */
void print_statistics(unsigned char *array, unsigned int size);

/**
 * @brief Print an array
 *
 * This function takes an array of 8-bit unsigned integers and simply prints out the values
 * in it linearly to the screen.
 *
 * @param array An array of unsigned char values which is to be printed
 * @param size Number of elements in the parameter array
 *
 * @return None
 */
void print_array(unsigned char *array, unsigned int size);

/**
 * @brief Finds the median value of an array
 *
 * This function takes an array of 8-bit unsigned integers, computes the median, and returns the
 * resultant median. In order to find the median, we need to have a sorted list of numbers.
 * Hence, this function performs sorting using sort_array() function before finding median but this
 * may modify the original array. So, it makes a copy of the original array called copy_of_array
 * and performs required operations on that copy, not on the original array.
 *
 * @param array An array of unsigned char values from which the median has to be computed
 * @param size Number of elements in the parameter array
 *
 * @return The median of the array of unsigned chars
 */
unsigned char find_median(unsigned char *array, unsigned int size);

/**
 * @brief Finds the mean value of an array
 *
 * This function takes an array of 8-bit unsigned integers, computes the mean,
 * and returns the resultant mean.
 *
 * @param array An array of unsigned char values from which the mean has to be computed
 * @param size Number of elements in the parameter array
 *
 * @return The mean of the array of unsigned chars
 */
unsigned char find_mean(unsigned char *array, unsigned int size);

/**
 * @brief Finds the largest element of an array
 *
 * This function takes an array of 8-bit unsigned integers, finds the largest,
 * and returns it.
 *
 * @param array An array of unsigned char values from which the largest element has to be found
 * @param size Number of elements in the parameter array
 *
 * @return The largest element of the array of unsigned chars
 */
unsigned char find_maximum(unsigned char *array, unsigned int size);

/**
 * @brief Finds the smallest element of an array
 *
 * This function takes an array of 8-bit unsigned integers, finds the smallest,
 * and returns it.
 *
 * @param array An array of unsigned char values from which the smallest element has to be found
 * @param size Number of elements in the parameter array
 *
 * @return The smallest element of the array of unsigned chars
 */
unsigned char find_minimum(unsigned char *array, unsigned int size);

/**
 * @brief Sort an array in decreasing order
 *
 * This function takes an array of 8-bit unsigned integers and then sorts the elements(using Bubble Sort algorithm)
 * in decreasing order, i.e, the first(0th) element will the largest and the last(n-1th) element will be the smallest.
 * The modifications on the array are reflected back since it was passed by reference.
 *
 * @param array An array of unsigned char values from which has to be sorted
 * @param size Number of elements in the parameter array
 *
 * @return None
 */
void sort_array(unsigned char *array, unsigned int size);

#endif /* __STATS_H__ */
