# Author: Irak Rigia

# Project Description
This project is basically the Week-1 assignment of the Coursera course
	"Introduction to Embedded Systems Software and Development Environments"
In here, we develop a simple application that performs statistical analytics on a dataset.
