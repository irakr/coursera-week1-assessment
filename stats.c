/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief Week-1 assessment
 *
 * Basic application to perform statistical analytics on a dataset.
 *
 * @author Irak Rigia
 * @date 25/08/2017
 *
 */



#include <stdio.h>
#include <stdlib.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  print_statistics(test, SIZE);
}

void print_statistics(unsigned char *array, unsigned int size) {
  puts("\n(Input array)");
  print_array(array, size);
  puts("\n(Statistical data)");
  printf("%-10s%5c%5d\n", "Median", '=', find_median(array, size));
  printf("%-10s%5c%5d\n", "Mean", '=', find_mean(array, size));
  printf("%-10s%5c%5d\n", "Minimum", '=', find_minimum(array, size));
  printf("%-10s%5c%5d\n", "Maximum", '=', find_maximum(array, size));
  sort_array(array, size);
  puts("\n(Sorted array)");
  print_array(array, size);
}

void print_array(unsigned char *array, unsigned int size) {
  unsigned int i;
  
  printf("Array[] =");
  for(i = 0; i < size; i++) {
    if(!(i % 16))
        puts("");
    printf("%4d", array[i]);
  }
  puts("");
}

unsigned char find_median(unsigned char *array, unsigned int size) {
  unsigned char *copy_of_array;
  unsigned int i;
  unsigned char median;
  
  // Make a copy of the original array
  copy_of_array = malloc(size*sizeof(char));
  if(!copy_of_array) {
    fprintf(stderr, "[Fatal error] In function find_median() when calling malloc()\n");
    exit(1);
  }
  
  // Copy contents
  for(i = 0; i < size; i++)
    copy_of_array[i] = array[i];
  
  // Perform operations on the copy
  
  sort_array(copy_of_array, size);
  
  median = ((size + 1) / 2) - 1;
  
  return copy_of_array[median];
}


unsigned char find_mean(unsigned char *array, unsigned int size) {
  unsigned int i;
  unsigned char mean;
  unsigned int sum = 0; // Since 'mean' alone can be used but is limitied to 2^8=256 values only
  
  for(i = 0; i < size; i++) {
    sum += array[i];
  }
  mean = sum / size;
  
  return mean;
}


unsigned char find_maximum(unsigned char *array, unsigned int size) {
  unsigned int i;
  unsigned char max = 0;
  
  for(i = 0; i < size; i++) {
    if(array[i] > max)
      max = array[i];
  }
  
  return max;
}

unsigned char find_minimum(unsigned char *array, unsigned int size) {
  unsigned int i;
  unsigned char min = array[0];
  
  for(i = 1; i < size; i++) {
    if(array[i] < min)
      min = array[i];
  }
  
  return min;
}

void sort_array(unsigned char *array, unsigned int size) {
  unsigned int i, j;
  
  for(i = 0; i < (size - 1); i++) {
    for(j = 0; j < ((size - 1) - i); j++) {
      if(array[j] < array[j+1]) {
        // Swap array[j] and array[j+1] contents
        unsigned char temp;
        temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
      }
    }
  }
}
